# coding: UTF-8
# サッカーボード中継・リプレイシステム メインプログラム
import helper
import play
import goal
import replay
import sys
import os
import shutil

# ウィンドウ作成
window_name = helper.createWindow()

# 中継→ゴール→リプレイ の繰り返し回数
LoopNum = 2

# 引数チェック
if len(sys.argv) < 2:
    sys.exit("Usage:\n  %s <device-address>" % sys.argv[0])
devAddr = sys.argv[1]

for i in range(0,LoopNum):
    
    #録画先フォルダ
    recDir = './rec'
    
    if os.path.exists(recDir):
        #録画先のフォルダを消す
        shutil.rmtree(recDir)
        os.mkdir(recDir)
    
    # プレイ中（中継）
    play.play(window_name, recDir, devAddr)
    
    # ゴールアニメーション表示
    goal.showGoalAnimation(window_name)
    
    # リプレイ中
    replay.replay(window_name, recDir)

# ウィンドウ破棄 
helper.destroyWindow()
