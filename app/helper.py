# coding: UTF-8
# サブモジュール
# ウィンドウ作成・破棄処理
import cv2.cv as cv

# ウィンドウ作成
def createWindow():
    # ウィンドウ名
    WINDOW_NAME = "window"
    # ウィンドウ作成
    # サイズを変更出来るようにCV_WINDOW_NORMALを指定する
    cv.NamedWindow(WINDOW_NAME, cv.CV_WINDOW_NORMAL)
    # ウィンドウ位置・サイズ変更
    WINDOW_POS_X = 0
    WINDOW_POS_Y = 0
    WINDOW_WIDTH = 640
    WINDOW_HEIGHT = 480
    cv.MoveWindow(WINDOW_NAME, WINDOW_POS_X, WINDOW_POS_Y) 
    cv.ResizeWindow(WINDOW_NAME, WINDOW_WIDTH, WINDOW_HEIGHT) 

    return WINDOW_NAME


# ウィンドウ破棄
def destroyWindow():
    cv.DestroyAllWindows()

