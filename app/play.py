# coding: UTF-8
# サブモジュール
# プレイ中の中継・録画処理

import bluepy.btle as btle
import cv2.cv as cv
import time
import os.path
import math
import struct

def play(window_name, recDir, devAddr):

    N = 10 # バッファ用に用意する画像の数
    frameCnt = 0 # 今のフレーム番号
    frameCntMax = 10 #フレーム番号の最大値。この値に達したら、frameCntをクリアしてその動画ファイルへの書き込みは終了させて次の動画ファイルへ書き込む。
                    # １フレームあたり約100ms程度要するので、10フレームで約１秒分になる。
    fileNum = 0 # 今のファイル番号
    fileNumMax = 5 # ファイル番号最大値。fileNumがこの値を超えたら、再びfileNum=0にリセットし、ファイル番号0のファイルに上書きしていく。

    # カメラ画像キャプチャのためのインスタンス生成
    cap = [ cv.CaptureFromCAM(0), #カメラ１
            cv.CaptureFromCAM(1)] #カメラ２
                    
    # キャプチャ画像サイズの設定
    for c in cap:
        cv.SetCaptureProperty(c, cv.CV_CAP_PROP_FRAME_WIDTH, 320)
        cv.SetCaptureProperty(c, cv.CV_CAP_PROP_FRAME_HEIGHT, 240)
    
    timeOld = 0;
    
    # 録画用インスタンス生成
    videoWriters = __createVideoWriters(fileNum, recDir)
    
    # ペリフェラルへ接続
    addrType = btle.ADDR_TYPE_RANDOM
    conn = btle.Peripheral(devAddr, addrType)
    
    isGoal = __judgeGoal(conn)

    judgeTime = 0
    
    # フレーム処理
    while True:

        # カメラ画像キャプチャ
        imgs = [ cv.QueryFrame(cap[0]),  #カメラ１でキャプチャしたイメージ
                 cv.QueryFrame(cap[1])]  #カメラ２でキャプチャしたイメージ
        # 画面へのライブ映像表示（カメラ１）
        cv.ShowImage(window_name, imgs[0])
    
        # 次の動画ファイルに進むか判断
        frameCnt += 1
        if frameCnt > frameCntMax:
            #一定フレーム数に達していたら保存先ファイルを変えて録画用インスタンスを再作成する
            frameCnt = 0
            # 動画番号更新
            fileNum = fileNum + 1
            if fileNum > fileNumMax:
                fileNum = 0
            # 今の録画用インスタンスを破棄して動画ファイルをクローズ
            __deleteVideoWriters(videoWriters)
            # 録画用インスタンス再作成
            videoWriters = __createVideoWriters(fileNum, recDir)

        # 録画
        for i, vw in enumerate(videoWriters):
            cv.WriteFrame(vw, imgs[i])
            pass
    
        # ゴール有無チェック
        # 1000msに１回チェック
        isGoal = False
        curTime = int(time.time() * 1000)
        if curTime > judgeTime:
            judgeTime = curTime + 1000
            isGoal = __judgeGoal(conn)
    
        if isGoal:
            # ゴール検出したら抜ける
            break
    
        # キーボード入力チェック
        if cv.WaitKey(1) > 0:
            # キーボード入力があれば抜ける
            break
    
        # １フレーム分の処理時間測定
        timeCur = int(time.time() * 1000)
#        print "time diff = %d [msec]" % (timeCur-timeOld)
        timeOld = timeCur

    print "finalize"
    __deleteVideoWriters(videoWriters)

    #録画終了時のファイル番号を書き込む
    f = open(recDir + '/fileNum.txt', 'w') # 書き込みモードで開く
    f.write("%d" % fileNum) # 引数の文字列をファイルに書き込む
    f.close() # ファイルを閉じる
    f = None

    # 接続切断
    conn.disconnect()


    

# 録画用インスタンス生成
def __createVideoWriters(fileNum, recDir):
    if os.path.exists(recDir):
        pass
    else:
        os.mkdir(recDir)

    # 録画データの縦横サイズ
    IMAGE_WIDTH = 320
    IMAGE_HEIGHT = 240
    # 録画データのFPS
    # (ファイル情報に書き込む値。実際にそのFPSで書き込めるかは別)
    FPS = 100
    # コーデック指定
    FOURCC = cv.CV_FOURCC('I','4','2','0')
    newVideoWritersList = []
    # 録画用インスタンス生成（カメラ２つ分作成）
    for i in range(0,2):
        #新たな画像ファイル作製
        savePath = recDir + "/%d_%d.avi" % (i, fileNum)
        newVW = cv.CreateVideoWriter(savePath, FOURCC, FPS, (IMAGE_WIDTH, IMAGE_HEIGHT))
        newVideoWritersList.append(newVW)

    # インスタンスを格納したリストを返却
    return newVideoWritersList

# 録画用インスタンス破棄
def __deleteVideoWriters(videoWriters):
    for i, vm in enumerate(videoWriters):
        # インスタンス破棄するとファイルが自動でクローズされる
        videoWriters[i] = None

# ゴール有無判定
def __judgeGoal(conn):
    # xzyいずれかの絶対値がしきい値オーバーしたときにゴールと判断する
    # ２つあるセンサーのどちらかがゴールしたときTrueを返す。

    sensorsVal = readSensorVal(conn)
    THRESHOLD = 1000
    for i in range(0,2):
        for j in range(0,3):
            if math.fabs(sensorsVal[i][j]) > THRESHOLD:
                print("Goal {}".format(i+1))
                return True
    return False

# ペリフェラルから読み取った値をデコードしセンサー値を得る
def __decodeSensorVal(data, data_st_ind):
    # x, z, y, の値を取得
    # 1軸あたり2バイトの符号ありデータ。xzyの順で格納。ビッグエンディアン。
    xzy = struct.unpack(">hhh", data[data_st_ind:data_st_ind+6])
    x = xzy[0]
    z = xzy[1]
    y = xzy[2]

    # センサ番号を判断
    if data_st_ind == 0:
        num = 1
    else:
        num = 2
    print "[Sensor %d] X:%+5d, Z:%+5d, Y:%+5d" % (num, x, z, y)

    return xzy

# ペリフェラルにREAD要求を発行し、センサー値を取得する
def readSensorVal(conn):
    # 全サービスを取得
    for svc in conn.getServices():
        #サービス内のキャラクタリスティック取得
        for ch in svc.getCharacteristics():
            # 所望のUUIDに一致するものを探す
            if ch.uuid == '2c810002-2949-42e2-8368-75646ab7e134':
                # 一致するものが見つかれば、READ要求を発行
                read_val = ch.read()
                # センサー値としての表示
                sensorsVal = [  __decodeSensorVal(read_val, 0),
                                __decodeSensorVal(read_val, 6)]

    return sensorsVal


## 単体テスト用メイン
if __name__ == "__main__":
    # ウィンドウ作成
    import helper
    window_name = helper.createWindow()

    import sys
    if len(sys.argv) < 2:
        sys.exit("Usage:\n  %s <device-address>" % sys.argv[0])
    devAddr = sys.argv[1]

    import shutil
    recDir = './rec'
    if os.path.exists(recDir):
        #録画先のフォルダを消す
        shutil.rmtree(recDir)
        os.mkdir(recDir)

    # プレイ中
    play(window_name, recDir, devAddr)

    # ウィンドウ破棄
    cv.DestroyAllWindows()

