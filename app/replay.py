# coding: UTF-8
# サブモジュール  
# リプレイ処理

import cv2.cv as cv
import time

# 録画をリプレイする
def replay(windowName, recDir):
    __replayOneCameraMovie(windowName, 0, recDir) #カメラ１に対してリプレイ
    __replayOneCameraMovie(windowName, 1, recDir) #カメラ２に対してリプレイ

# 動画ファイル読み込み
def __openMovieFiles(fileNum, camNumber, recDir):
    return cv.CaptureFromFile(recDir + "/%d_%d.avi" % (camNumber, fileNum))

# 最後のファイル番号を取得
def __readLastFileNum(recDir):
    f = open(recDir + "/fileNum.txt", "r")
    v = 0
    for line in f:
        v = line
    print "num:%d\n" % int(v)
    return int(v)

# 1つのカメラの録画をリプレイする
def __replayOneCameraMovie(windowName, camNumber, recDir):
    frameCnt = 0 # 今のフレーム番号
    fileNum = __readLastFileNum(recDir) + 1 #ファイルに書かれている番号の次の番号が開始ファイル番号

    readedFileNumFlg = [False] * 256 #ファイル番号ごとの読み込み済みフラグ保持用

    #　初回動画ファイル読み込み
    cap = __openMovieFiles(fileNum, camNumber, recDir)
    # フレーム処理
    while True:
        frameCnt += 1
        print "frameCnt:%d, fileNum:%d" % (frameCnt, fileNum)

        img = cv.QueryFrame(cap)
        if img != None:
            # １フレーム分の画像が取得出来た場合
            #  録画画像を表示する
            
            # 画像の左上にリプレイ中のラベルをオーバーレイ
            backPosLeftTop = (0,0)
            backPosRightBottom = (125, 35)
            backColorBGR = (255, 255, 255)
    	    cv.Rectangle(img, backPosLeftTop, backPosRightBottom, backColorBGR,-1)
    	    font = cv.InitFont(cv.CV_FONT_HERSHEY_SIMPLEX,1.0,1.0,shear=0, thickness=2, lineType=8) 
    	    textColorBGR = (0,0,255)
    	    textPosLeftTop = (5,30)
     	    cv.PutText(img,"REPLAY",textPosLeftTop,font,textColorBGR) 
            # 録画画像表示
            cv.ShowImage(windowName, img)

        else:
            # １フレーム分の画像が取れなかった場合
            # 末尾に達した or 動画ファイルの読み込みに失敗したと判断する。
            
            readedFileNumFlg[fileNum] = True #このファイル番号は終わったことを示すフラグ立てる
            if readedFileNumFlg[fileNum+1] :
                #次のファイル番号を既に再生ずみなら、全てのファイルを再生し終えたということ
                #ループを抜ける
                break

            if frameCnt == 1:
                # 1フレームも読み込む事無く、いきなり画像取得できなかったということは、ファイル自体が存在しないということ。
                # 末尾のファイル番号の次の番号を読み込もうとしたケースとみなし
                # ファイル番号０から読みなおしてみる。
                frameCnt = 0
                fileNum = 0
                cap = None
                cap = __openMovieFiles(fileNum, camNumber, recDir)
            else :
                # 何フレームか読み込んだ後で画像取得できなかったということは、末尾に達したとみなし、
                # 次のファイル番号に切り替える
                frameCnt = 0
                fileNum = fileNum + 1
                cap = None
                cap = __openMovieFiles(fileNum, camNumber, recDir)
                print "frame end. next..."

        #キー入力チェック
        if cv.WaitKey(1) > 0:
            break

        #フレームレート分の待ち
        #スローモーション再生になるよう録画時のフレームレートよりやや長めに待つ
        time.sleep(0.2)


## 単体テスト用メイン
if __name__ == "__main__":
    # ウィンドウ作成
    import helper
    window_name = helper.createWindow()
    recDir = './rec'

    # リプレイ中
    replay(window_name, recDir)
