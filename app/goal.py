# coding: UTF-8
# サブモジュール
# ゴール検出時のアニメーション表示

import cv2.cv as cv
import time

# ゴール時アニメーションを表示
# ２枚の静止画を交互に表示することでアニメーションさせる
def showGoalAnimation(windowName):
    # 画像取得
    img = [ cv.LoadImage("./img/goal_1.png"),
            cv.LoadImage("./img/goal_2.png")]
    # アニメーションの長さ[ms]
    animationTimeLength = 3000
    # アニメーション終了時刻
    animationEndTime = int(time.time()*1000) + animationTimeLength
    # ループカウンタ
    cnt = 0
    # アニメーション終了時刻に達するまでループ
    while int(time.time()*1000)<animationEndTime:
        # 画像表示(カウンタを使って画像を交互に切り替える）
        cv.ShowImage(windowName,img[cnt%2])
        # キーボード入力チェック
        if cv.WaitKey(1) > 0:
            # キーボード入力があれば抜ける
            break
        # １フレーム分の待ち
        time.sleep(0.1)
        # カウンタ更新
        cnt += 1


## 単体テスト用メイン
if __name__ == '__main__':
    # ウィンドウ作成
    import helper
    window_name = helper.createWindow()
    showGoalAnimation(window_name)
