# coding: UTF-8
# BLE Nanoと通信しHMC5883Lセンサ値を取得・表示

import bluepy.btle as btle
import sys
import binascii
import time
import ctypes
import struct

def printSensorVal(data, data_st_ind):
    # x, z, y, の値を取得
    # 1軸あたり2バイトの符号ありデータ。xzyの順で格納。ビッグエンディアン。
    xzy = struct.unpack(">hhh", data[data_st_ind:data_st_ind+6])
    x = xzy[0]
    z = xzy[1]
    y = xzy[2]
    
    # センサー番号
    if data_st_ind == 0:
        num = 1
    else:
        num = 2
    
    print "[Sensor %d] X:%+5d, Z:%+5d, Y:%+5d" % (num, x, z, y)


if len(sys.argv) < 2:
    sys.exit("Usage:\n  %s <device-address>" % sys.argv[0])

# ペリフェラルへ接続
devAddr = sys.argv[1]
addrType = btle.ADDR_TYPE_RANDOM
conn = btle.Peripheral(devAddr, addrType)

while True:
    # 全サービスを取得
    for svc in conn.getServices():
        #サービス内のキャラクタリスティック取得
        for ch in svc.getCharacteristics():
            # 所望のUUIDに一致するものを探す
            if ch.uuid == '2c810002-2949-42e2-8368-75646ab7e134':
                # 一致するものが見つかれば、READ要求を発行
                read_val = ch.read()
                # サービスとキャラクタリスティックのUUID表示
                print("Service: uuid:{}".format(svc.uuid))
                print(" Chara.: uuid:{}".format(ch.uuid))
                # キャラクタリスティックのREAD要求結果を表示
                print("         read value > 0x{}".format(binascii.b2a_hex(read_val)) )
                # センサー値の表示
                print("\r")
                printSensorVal(read_val, 0)
                printSensorVal(read_val, 6)
                print("\r")

    time.sleep(1)
    
# 接続切断
conn.disconnect()
