# coding: UTF-8
# USBカメラの映像表示・録画サンプル
# （カメラ２つ）

import cv2.cv as cv
import time
import os.path

# 録画用インスタンス生成
def createVideoWriters():
    if not os.path.exists("./rec"):
        os.mkdir("./rec")
    # 録画データの縦横サイズ
    IMAGE_W = 320
    IMAGE_H = 240
    # 録画データのFPS
    # (ファイル情報に書き込む値。
    #  実際のFPSはキャプチャ間隔に依存)
    FPS = 100
    # コーデック指定
    FOURCC = cv.CV_FOURCC('I','4','2','0')
    videoWriters = []
    # 録画用インスタンス生成（カメラ２つ分作成）
    for i in range(0,2):
        #録画先ファイルのパス
        savePath = "./rec/%d.avi" % (i)
        #インスタンス生成
        newVW = cv.CreateVideoWriter(savePath,
                   FOURCC, FPS, 
                   (IMAGE_W, IMAGE_H))
        videoWriters.append(newVW)
    # インスタンスを格納したリストを返却
    return videoWriters

# 録画用インスタンス破棄
def deleteVideoWriters(videoWriters):
    for i, vm in enumerate(videoWriters):
        # インスタンス破棄すると
        # ファイルが自動でクローズされる
        videoWriters[i] = None

# ウィンドウ名
WINDOW_NAME = "window"
# ウィンドウ作成
cv.NamedWindow(WINDOW_NAME, cv.CV_WINDOW_NORMAL)
# ウィンドウ位置設定(x=0,y=0)
cv.MoveWindow(WINDOW_NAME, 0, 0) 
# ウィンドウサイズ設定(幅640,高さ480)
cv.ResizeWindow(WINDOW_NAME, 640,480) 
# カメラ画像キャプチャのためのインスタンス生成
cap = [ cv.CaptureFromCAM(0), #カメラ１
        cv.CaptureFromCAM(1)] #カメラ２
# キャプチャ画像サイズの設定
for c in cap:
    cv.SetCaptureProperty(c, 
          cv.CV_CAP_PROP_FRAME_WIDTH, 320)
    cv.SetCaptureProperty(c, 
          cv.CV_CAP_PROP_FRAME_HEIGHT, 240)

timeOld = 0;

# 録画用インスタンス生成
videoWriters = createVideoWriters()
# フレーム処理
while True:
    # カメラ画像キャプチャ
    imgs = [ cv.QueryFrame(cap[0]), #カメラ１の画像
             cv.QueryFrame(cap[1])] #カメラ２の画像
    # 画面へのライブ映像表示（カメラ１）
    cv.ShowImage(WINDOW_NAME, imgs[0])
    # 録画
    for i, vw in enumerate(videoWriters):
        cv.WriteFrame(vw, imgs[i])
    # キーボード入力チェック
    if cv.WaitKey(1) > 0:
        # キーボード入力があれば抜ける
        break
#終了処理
deleteVideoWriters(videoWriters)
cv.DestroyAllWindows()
