# coding: UTF-8
# 動画を再生する

import cv2.cv as cv
import time

# ウィンドウ名
WINDOW_NAME = "window"
# ウィンドウ作成
cv.NamedWindow(WINDOW_NAME, cv.CV_WINDOW_NORMAL)
# ウィンドウ位置・サイズ変更
WINDOW_POS_X = 0
WINDOW_POS_Y = 0
WINDOW_WIDTH = 640
WINDOW_HEIGHT = 480
cv.MoveWindow(WINDOW_NAME, WINDOW_POS_X, WINDOW_POS_Y) 
cv.ResizeWindow(WINDOW_NAME, WINDOW_WIDTH, WINDOW_HEIGHT) 

# 動画ファイル読み込み
cap = cv.CaptureFromFile("rec/0.avi")

#フレームレート数取得
#(ただし、実際の録画FPSが一致しているとは限らないので、特に使わない)
framerate =  cv.GetCaptureProperty(cap, cv.CV_CAP_PROP_FPS)
print "frame rate : %f" % (framerate)

timeOld = 0;

#フレーム処理
while True:
    # 1フレーム分の画像を取得
    img = cv.QueryFrame(cap)
    if img != None:
        # 画像が取得出来た場合
        # ウィンドウに表示
        cv.ShowImage(WINDOW_NAME, img)
    else:
        # 画像が取得出来なかった場合
        # 全フレーム取得済みと判断して、終了
        break;
    
    # キー入力チェック
    if cv.WaitKey(1) > 0:
        break
        
    # １フレーム分の処理時間測定
    timeCur = int(time.time() * 1000)
    print "time diff = %d [msec]" % (timeCur-timeOld)
    timeOld = timeCur
        
cv.DestroyAllWindows()

